<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini
        for($i=0; $i<=20; $i+=2){
            echo $i . " - I Love PHP <br>";
        }

        echo "<br><br>";

        for($i=20; $i>=0; $i-=2){
            echo $i . " - I love PHP <br>";
        }

        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = ["angka ke-1" => 18, "angka ke-2" =>45, "angka ke-3" => 29, "angka ke-4" => 61, "angka ke-5" => 47, "angka ke-6" => 34];
        echo "<b><pre> array numbers: ";
        print_r($numbers);
        echo "<br></b></pre>";
        foreach ($numbers as $index){
            $jenisangka = $index%5;
            $rest[] = $index%5;

            if($jenisangka==0){
                echo "hasil bagi : ";
                echo $jenisangka;
                echo " maka dari itu angka yang habis di bagi 5 <br>";
            }
            else{
                echo "hasil bagi : ";
                echo $jenisangka;
                echo " maka dari itu angka yang tidak habis di bagi 5 <br>";
            }
        };
        echo "<br>";
        echo "hasil sisa bagi :";
        echo "<pre>";
        print_r ($rest);
        echo "</pre>";


        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        // Output: 
        foreach($items as $index => $val){
            $items = [
                "ID :" => $val[0],
                "Nama Barang: " => $val[1],
                "Harga : Rp " => $val[2],
                "Deskrispi : " => $val[3],
                "image : " => $val[4]
            ];
                foreach($items as $judulindex => $isivalue){
                    echo "$judulindex  $isivalue <br>";
                }
                echo "<br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */
        echo "Asterix: <br>";
        for ($i=0; $i<=5; $i++){
            for($j=0; $j<=$i; $j++){
                echo "*";
            }
            echo "<br>";
        }                
    ?>

</body>
</html>